#lang racket

(require crypto crypto/libcrypto
         web-server/servlet
         web-server/servlet-env
         json net/url
         "memory-store.rkt"
         "sqlite-store.rkt"
         "install-factory.rkt"
         db)

(define db
  (make-parameter #f))

(provide db start)

(define (not-found)
  (response 404 #"Not Found"
            (current-seconds) #"text/plain"
            '()
            (lambda (out-port)
              (display #"404 Not Found" out-port))))

(define (ok body #:content-type [content-type #"application/octet-stream"])
  (response
   200 #"OK"
   (current-seconds) content-type
   '()
   (lambda (out-port)
     (write-bytes body out-port))))

(define (start request)
  (match (request-method request)
    [#"GET"
     (match (assoc 'xt (url-query (request-uri request)))
       [#f (not-found)]
       [(cons 'xt uri-to-get)
        (match (send (db) get (string->url uri-to-get))
          [#f (not-found)]
          [(? bytes? out-bytes)
           (ok out-bytes)])])]
    [#"POST"
     (match (request-post-data/raw request)
       [(? bytes? in-bytes)
        (define cas-uri
          (send (db) put! in-bytes))
        (ok (string->bytes/latin-1 (url->string cas-uri))
            #:content-type #"text/plain")])]))

(define (main)
  (define sqlite-path #f)
  (define this-db #f)
  (command-line
   #:program "cas-server"
   #:once-each
   [("-s" "--sqlite-path")
    sqlite-path-option
    ("Filename of path to save sqlite file")
    (set! sqlite-path sqlite-path-option)]
   #:args ()
   (begin
     (cond
       [sqlite-path
        (define existed?
          (file-exists? sqlite-path))
        (set! this-db
              (new sqlite-store%
                   [conn (sqlite3-connect
                          #:database (string->path sqlite-path)
                          #:mode 'create)]))
        (unless existed?
          (send this-db init!))]
       [else
        (set! this-db (new memory-store%))])
     (parameterize ([db this-db])
       (serve/servlet start
                      #:servlet-regexp #rx""
                      #:launch-browser? #f)
       (void)))))

(module+ main
  (install-default-factories!)
  (main))
