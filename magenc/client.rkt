#lang racket

(require crypto
         net/url
         "get-put.rkt"
         "web-store.rkt"
         "debug-store.rkt"
         "sha256d.rkt")

(define (get-it store magnet-url out-filename)
  (define out-bytes
    (call-with-output-bytes
     (lambda (p)
       (magenc-get p magnet-url store))))
  (if out-filename
      (begin
        (when (file-exists? out-filename)
          (delete-file out-filename))
        (call-with-output-file out-filename
          (lambda (p)
            (write-bytes out-bytes p))))
      (write-bytes out-bytes))
  (void))

(define (post-it store filename what-kind-of-key)
  (define key
    (match what-kind-of-key
      ['random (generate-cipher-key '(aes ctr)
                                    #:size 32)]
      ['convergent (call-with-input-file filename
                     (lambda (p)
                       (digest 'sha256 bytes)))]))
  (define new-url
    (call-with-input-file filename
      (lambda (p)
        (magenc-put! p store key))))
  (displayln (url->string new-url)))

(module+ main
  (require "install-factory.rkt")
  (install-default-factories!)

  (define verbose #f)
  (define out-filename #f)
  (define filename #f)
  (define magnet-url #f)
  (define what-kind-of-key
    'random)
  (command-line
   #:program "magenc"
   #:once-each
   [("-v" "--verbose") "Display verbose output"
                       (set! verbose #t)]
   [("-o" "--output") out-filename-option
                      ("Filename to save"
                       "contents fetched with --get")
                      (set! out-filename out-filename-option)]
   #:once-any
   [("-u" "--upload") filename-option
                      "Upload file to server"
                      (set! filename filename-option)]
   [("-g" "--get") magnet-url-option
                   ("Fetch and decrypt"
                    "an object at this magnet url")
                   (set! magnet-url magnet-url-option)]
   #:once-any
   [("-c" "--convergent") ("Use convergent encryption to generate key from"
                           "file hash (default is a random key)")
                          (set! what-kind-of-key 'convergent)]
   #:args ([server-url "http://localhost:8000"])
   (let* ([store-class%
           (if verbose
               (debug-mixin web-store%)
               web-store%)]
          [store
           (new store-class%
                [server-url (string->url server-url)])])
     (cond
       [(not (or filename magnet-url))
        (displayln "One of --upload or --get arguments must be provided")
        (exit 1)]
       [filename
        (post-it store filename what-kind-of-key)
        (void)]
       [magnet-url
        (get-it store (string->url magnet-url)
                out-filename)
        (void)]))))
