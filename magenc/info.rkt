#lang info

(define deps '("base" "crypto-lib" "csexp" "db-lib" "gui-lib" "sql" "web-server-lib"))
(define build-deps '("rackunit-lib" "scribble-lib"))
(define pkg-desc "Add symmetric encryption to magnet URIs")
(define version "0.0")
(define pkg-authors '("cwebber"))
(define raco-commands '(("magenc" (submod magenc/client main)
                                  "Run magnetic encyprion client"
                                  #f)
                        ("cas-server" (submod magenc/cas-server main)
                                      "Run Content Addressed Server demo"
                                      #f)))
(define scribblings '(("scribblings/magenc.scrbl")))
